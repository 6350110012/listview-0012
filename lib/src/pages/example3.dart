


import 'package:flutter/material.dart';

class Example3 extends StatelessWidget {
  Example3({Key? key}) : super(key: key);

  final titles = ['Cake ', 'Donut', 'Ice cream', 'Bread',
    'Milk', 'Tea', 'Coffee', 'Cookies'];

  final subtitle = ['เค้ก', 'โดนัท', 'ไอศกรีม', 'ขนมปัง',
    'นม', 'ชา', 'กาแฟ', 'คุกกี้'];

  final image = [
    NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTrZeen8GHeUPjU54PP08k8XKeQK1ipSHEXfA&usqp=CAU'),
    NetworkImage('https://thumbs.dreamstime.com/b/three-donuts-vector-illustration-glaze-sprinkles-white-background-54595261.jpg'),
    NetworkImage('https://w7.pngwing.com/pngs/635/1022/png-transparent-blue-ceramic-bowl-with-ice-cream-in-it-chocolate-ice-cream-milkshake-ice-cream-cone-ice-cream-free-cream-food-free-logo-design-template-thumbnail.png'),
    NetworkImage('https://e7.pngegg.com/pngimages/542/368/png-clipart-bread-bread.png'),
    NetworkImage('https://i.pinimg.com/736x/f4/55/06/f45506107942a8db26360deb678c4f5f.jpg'),
    NetworkImage('https://chillchilljapan.com/wp-content/uploads/2021/08/pixta_31540845_M-760x570.jpg'),
    NetworkImage('https://e7.pngegg.com/pngimages/839/538/png-clipart-creative-coffee-coffee-open-beans.png'),
    NetworkImage('https://toppng.com/uploads/preview/chocolate-chip-cookies-chocolate-chip-cookie-11563621781trcwdhfgac.png'),

  ];
  // final icons = [Icons.directions_bike, Icons.directions_boat,
  //  Icons.directions_bus, Icons.directions_car, Icons.directions_railway,
  //  Icons.directions_run, Icons.directions_subway, Icons.directions_transit,
  //  Icons.directions_walk];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Bakery'),
        ),
        body: ListView.builder(
            itemCount: titles.length,
            itemBuilder: (context,index){
              return Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(backgroundImage: image[index],),
                    title: Text('${titles[index]}', style: TextStyle(fontSize: 18),),
                    subtitle: Text('${subtitle[index]}',style: TextStyle(fontSize: 15),),
                    trailing: Icon(Icons.notifications_none,size: 25,),
                    onTap: (){
                      BuildContext dialogContext;
                      showDialog(context: context, builder: (BuildContext context){
                        dialogContext = context;
                        return AlertDialog(
                          title: Text('WELCOME'),
                          content: Text('Hello ${titles[index]}'),
                          actions: [
                            ElevatedButton (
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text('CANCEL'),),
                            ElevatedButton (
                              onPressed: () {  },
                              child: Text('ACCEPT'),)
                          ],
                        );

                      });

                    },
                  ),
                  Divider(thickness: 1,)
                ],
              );
            }
        )
    );
  }
}
